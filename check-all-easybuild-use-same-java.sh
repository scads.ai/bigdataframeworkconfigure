#!/bin/bash
java_dependency=`grep Java Hadoop.eb`

all_ok=true

for i in `git ls-files -- '*.eb'`
do
	echo "Checking $i..."
	if grep -q "$java_dependency" $i
	then
		echo "  Ok!"
	else
		all_ok=false
		echo "  Fail!"
	fi
	echo ""
done

if ! $all_ok
then
	exit 1
fi
