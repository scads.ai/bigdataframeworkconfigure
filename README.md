# BigDataFrameworkConfigure

Install [EasyBuild modules](https://github.com/easybuilders/easybuild-easyconfigs) for Hadoop, Spark, Flink and Kafka, as well as scripts for their configuration via make.

## Description

This projects aims to provide a Makefile-based installation for [Apache Hadoop](https://hadoop.apache.org/), [Apache Spark](https://spark.apache.org/), [Apache Flink](https://flink.apache.org/) and [Apache Kafka](https://kafka.apache.org/) on High-Performance compute clusters managed with [EasyBuild](https://easybuild.io/). This project assumes, that nodes are allocated dynamically via the [Slurm](https://slurm.schedmd.com/) scheduling system. Thus, this project also provides an EasyBuild module `BigDataFrameworkConfigure` to generate configurations for Hadoop, Spark, Flink, Kafka and perhaps other distributed frameworks dynamically.

## Installation

You need make and EasyBuild installed. On HPC clusters of ZIH, these tools are pre-installed.

## Usage

In an interactive session, load an EasyBuild module and run make. The following code snippet shows how to do that:

```bash
srun --pty --ntasks=1 --time=2:00:00 --mem=2000 bash -l
# Now wait for the allocation...
ml EasyBuild
make install-all
```

## Support

The hope is that this README.md is self-explanatory. Please [open issues](https://gitlab.hrz.tu-chemnitz.de/scads.ai/bigdataframeworkconfigure/-/issues) if you feel that an improvement is necessary.

## Contributing

Contributions are very welcome! Feel free to share ideas by opening issues or contributing code.

## License

Please see [the License](LICENSE).

## Project status

This project is active, but the maintainers have a lot of other things to do. Thus, improvements (bug fixes, new features) only appear from time to time. So, please contribute!
