#!/bin/bash
script=${BASH_SOURCE[0]}
bin=`dirname "$script"`
bin=`cd "$bin"; pwd`

function err_pr {
	echo "[ERROR] $*" >&2
	exit 1
}
function print_nodelist {
	scontrol show hostname $SLURM_NODELIST
}
if [[ $# -ne 1 ]]; then
	err_pr "usage: frameworks-cleanup.sh $FRAMEWORK_CONF_DIR"
fi
FRAMEWORK_CONF_DIR=$1
# include framework specific stuff here, such as files to modify (FRAMEWORK_CONF_FILES), file to enumerate slaves (FRAMEWORK_SLAVE_FILE), file to enumerate masters (FRAMEWORK_MASTER_FILE), path to scripts (FRAMEWORK_BIN_DIR)
local META_CONF=$FRAMEWORK_CONF_DIR/meta.conf
if [ -e $META_CONF ]; then
. $META_CONF
else
err_pr "No file 'meta.conf' in $FRAMEWORK_CONF_DIR"
fi
local FRAMEWORK_LOCAL_DIR=/tmp/$USER/cluster-conf-$SLURM_JOB_ID
local FRAMEWORK_DATA_DIR=${FRAMEWORK_DATA_DIR:-$FRAMEWORK_LOCAL_DIR}
FRAMEWORK_MASTER_NODE=$(print_nodelist | awk '{print $1}' | sort -u | head -n 1)
FRAMEWORK_SLAVE_NODES=$(print_nodelist | awk '{print $1}' | sort -u | sed ":a;N;s/\\n/ /g;ba")
#persist keyword replacements:
if [ "x$FRAMEWORK_DATA_DIR" = "x$FRAMEWORK_LOCAL_DIR" ]; then
	for node in $FRAMEWORK_MASTER_NODE $FRAMEWORK_SLAVE_NODES
	do
		srun --overlap -N1 -n1 --nodelist=$node "rm $FRAMEWORK_LOCAL_DIR"
	done
fi
