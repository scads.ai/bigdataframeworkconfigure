#!/bin/bash
LOGFILE="$1"
REQUIRED_NUMBER_OF_INSTANCES=$2
TO_GREP="$3"

GOON=true
while $GOON
do
    NUMREGISTERED=`grep -c "$TO_GREP" "$LOGFILE" 2>/dev/null`
    if [ "x$NUMREGISTERED" = "x$REQUIRED_NUMBER_OF_INSTANCES" ]; then
        GOON=false
    fi
done

