VERSION := $(shell cat versionfile)
.PHONY: easybuild-loaded assert-workspace-available install
.PRECIOUS: BigDataFrameworkConfigure-${VERSION}.eb bigdataframeworkconfigure-${VERSION}.tar.gz

spark_easybuilds := $(wildcard Spark*.eb)
spark_versions_install := $(spark_easybuilds:%.eb=install-%.eb)
spark_versions_test := $(spark_easybuilds:%.eb=test-%.eb)

workspace_name=frameworks

easybuild-loaded:
	@type eb > /dev/null 2>&1 || ( echo "Please run 'ml EasyBuild' before running 'make install'" ; exit 1 )

assert-workspace-available:
	ws_find ${workspace_name} > /dev/null 2>&1 || ws_allocate -F horse -d 100 -g -G p_scads ${workspace_name}

BigDataFrameworkConfigure-%.eb: bigdataframeworkconfigure-%.tar.gz BigDataFrameworkConfigure.eb.template Makefile
	CHECKSUM=`sha512sum $< | sed 's/^\([a-z0-9]*\).*/\1/'`; \
	sed "s/CHECKSUM/$${CHECKSUM}/;s/VERSION/${VERSION}/" BigDataFrameworkConfigure.eb.template > $@

clean-bigdataframeworkconfigure-data:
	rm -f BigDataFrameworkConfigure-${VERSION}.eb bigdataframeworkconfigure-${VERSION}.tar.gz bigdataframeworkconfigure-${VERSION}.tar.gz.sha512sum

bigdataframeworkconfigure-%.tar.gz: bigdataframeworkconfigure/framework-configure.sh bigdataframeworkconfigure/framework-cleanup.sh bigdataframeworkconfigure/wait-for-processes-started.sh Makefile
	tar czvf "$@" $(<D)
	sha512sum "$@" | awk '{print $$1}' > $@.sha512sum

install-%.eb: %.eb easybuild-loaded assert-workspace-available Makefile
	PIP_REQUIRE_VIRTUALENV=false; \
	module use /software/modules/$${LMOD_SYSTEM_NAME}/r24.04/all; \
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	mkdir -p $${WORKSPACE_PATH}/$${LMOD_SYSTEM_NAME}; \
	module use $${WORKSPACE_PATH}/$${LMOD_SYSTEM_NAME}/modules/all/Compiler/GCC/13.2.0; \
	eb --configfiles=eb-config-file --sourcepath=$${WORKSPACE_PATH}/$${LMOD_SYSTEM_NAME}/sources --installpath=$${WORKSPACE_PATH}/$${LMOD_SYSTEM_NAME} --robot-path="$${EASYBUILD_ROBOT_PATHS}:$$(find $${WORKSPACE_PATH}/$${LMOD_SYSTEM_NAME} -name "*.eb" -exec dirname {} \; | sort -u | tr '\n' ':')" $<

install: install-BigDataFrameworkConfigure-${VERSION}.eb

install-spark: install-Spark.eb

install-spark-versions: $(spark_versions_install)

install-flink: install-Flink.eb

install-kafka: install-Kafka.eb

install-hadoop: install-Hadoop.eb

install-all: install install-hadoop install-spark install-flink install-kafka

remove-workspace:
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	if [ -d "$${WORKSPACE_PATH}" ]; \
	then rm -rf $${WORKSPACE_PATH}/*; \
	ws_release -F horse ${workspace_name}; \
	fi

test-flink-in-slurm-job:
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	sbatch -J test-flink-kmeans test-run.sbatch test-flink-kmeans.example $${WORKSPACE_PATH}

test-flink-from-ci:
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	./test-flink-kmeans.example $${WORKSPACE_PATH}

test-Spark%eb: Spark%eb
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	export TEST_SPARK_VERSION=$$(sed -n '/^version/s/[^0-9]*\([0-9]*\.[0-9]*\.[0-9]*\)./\1/p' $<); \
	./test-spark-pi.example $${WORKSPACE_PATH} $${TEST_SPARK_VERSION}

test-spark-versions-from-ci: $(spark_versions_test)

test-kafka-from-ci:
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	./test-kafka.example $${WORKSPACE_PATH}

check-toolchain-dependency:
	./check-all-easybuild-use-same-toolchain.sh

check-java-dependency:
	./check-all-easybuild-use-same-java.sh

test-framework-configure-invocations-in-slurm-job:
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	sbatch -J test-framework-configure test-run.sbatch test-framework-configure $${WORKSPACE_PATH}

test-framework-configure-invocations-from-ci:
	export WORKSPACE_PATH=`ws_find -F horse ${workspace_name}`; \
	./test-framework-configure $${WORKSPACE_PATH}

version-inc:
	awk -F'.' '{printf("%s.%d.%d",$$1,$$2,$$3+1)}' versionfile > versionfile2
	mv versionfile2 versionfile
