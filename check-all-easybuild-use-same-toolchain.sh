#!/bin/bash
toolchain_version=`sed -n "/toolchain/s/toolchain = {'name': \?'GCC', \?'version': \?'\([^']*\)'}/\1/p" BigDataFrameworkConfigure.eb.template`

all_ok=true

for i in `git ls-files -- '*.eb'`
do
	echo "Checking $i..."
	if grep -q toolchain $i
	then
		if ! grep -v "toolchain = {'name': 'GCC', 'version': '$toolchain_version'}" $i | grep toolchain
		then
			echo "  Correct toolchain!"
		else
			all_ok=false
			echo "  Wrong toolchain!"
		fi
	else
		all_ok=false
		echo "  No toolchain!"
	fi
	echo ""
done

if ! $all_ok
then
	exit 1
fi
