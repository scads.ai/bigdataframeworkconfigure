#!/bin/bash

create_package() {
    local MY_VERSION=$1
    local MY_CI_TOKEN=$2

    # Deleting any pre-existing packages
    delete_package "$MY_VERSION" "$MY_CI_TOKEN"

    echo "Cleaning up files"
    make clean-bigdataframeworkconfigure-data VERSION=${VERSION}
    make ${BDFC_EASYBUILD_FILE_NAME} VERSION=${VERSION}
    make ${BDFC_TAR_FILE_NAME} VERSION=${VERSION}
    
    echo "Uploading assets to package registry"
    curl --header "PRIVATE-TOKEN: $MY_CI_TOKEN" --upload-file "${BDFC_TAR_FILE_NAME}" --url "${PACKAGE_REGISTRY_URL}/${MY_VERSION}/${BDFC_TAR_FILE_NAME}"
    curl --header "PRIVATE-TOKEN: $MY_CI_TOKEN" --upload-file "${BDFC_TAR_CHECKSUM_FILE_NAME}" --url "${PACKAGE_REGISTRY_URL}/${MY_VERSION}/${BDFC_TAR_CHECKSUM_FILE_NAME}"
    curl --header "PRIVATE-TOKEN: $MY_CI_TOKEN" --upload-file "${BDFC_EASYBUILD_FILE_NAME}" --url "${PACKAGE_REGISTRY_URL}/${MY_VERSION}/${BDFC_EASYBUILD_FILE_NAME}"
}

create_release() {
    local MY_VERSION=$1
    local MY_CI_TOKEN=$2
    
    echo "Updating for version: ${MY_VERSION}"
    
    echo "Get data from package registry: ${PACKAGE_REGISTRY_URL}/${MY_VERSION}/${BDFC_TAR_CHECKSUM_FILE_NAME}"
    wget "${PACKAGE_REGISTRY_URL}/${MY_VERSION}/${BDFC_TAR_CHECKSUM_FILE_NAME}"
    DESCRIPTION="$(cat ./${BDFC_TAR_CHECKSUM_FILE_NAME})"
    
    echo "Deleting the existing release candidate with same version, if exists"
    curl --request DELETE --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" --url "${RELEASE_URL}/${MY_VERSION}"
        
    echo "Updating tag location for the release version"
    curl --request DELETE --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" --url "${REPOSITORY_URL}/tags/${MY_VERSION}"
    curl --request POST --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" --url "${REPOSITORY_URL}/tags?tag_name=${MY_VERSION}&ref=${CI_COMMIT_REF_NAME}"

    echo "Pushing the release"
    curl --request POST \
        --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "{\"tag_name\": \"${MY_VERSION}\", \"title\": \"${MY_VERSION}\", \"name\": \"${MY_VERSION}\", \"description\":\"${DESCRIPTION}\",\"assets\":{\"links\":[{\"name\":\"${BDFC_TAR_FILE_NAME}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${MY_VERSION}/${BDFC_TAR_FILE_NAME}\",\"link_type\": \"other\"}]}}" \
        --url "${RELEASE_URL}"
}

delete_release() {
    local MY_VERSION=$1; local MY_CI_TOKEN=$2
    echo "Deleting the release, if exists"
    curl --request DELETE --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" --url "${RELEASE_URL}/${MY_VERSION}"
    echo "Deleting the tag"
    curl --request DELETE --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" --url "${REPOSITORY_URL}/tags/${MY_VERSION}"
}

delete_package() {
    local MY_VERSION=$1; local MY_CI_TOKEN=$2
    
    # Personal access token
    ACCESS_TOKEN="your_personal_access_token"
    
    # Get the package ID based on the version
    PACKAGE_ID=$(curl --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" "${PACKAGE_URL}" | \
        python3 -c "import sys, json; data=json.load(sys.stdin); print(next((item['id'] for item in data if item['version'] == '${VERSION}'), None))")

    if [[ "${PACKAGE_ID}" != "None" ]]; then
        echo "Package ID for version ${VERSION} is \"${PACKAGE_ID}\""
        # Curl command to delete the package
        echo "Deleting package for version \"${VERSION}\""
        curl --request DELETE --header "PRIVATE-TOKEN: ${MY_CI_TOKEN}" --url "${PACKAGE_URL}/${PACKAGE_ID}"
    else
        echo "No package was found for version: $VERSION"
    fi
}

main(){
    COMMAND=$1
    VERSION=$2
    MY_CI_TOKEN=${3:-''}
    
    #Checks
    if [[ -z "$VERSION" ]]; then
        echo "Please provide valid version as the 2nd argument."
    fi
    if [[ -z "$MY_CI_TOKEN" ]]; then
        echo "Please provide valid token as the 2nd argument."
    fi
    
    BDFC_EASYBUILD_PREFIX=BigDataFrameworkConfigure

    CI_API_V4_URL=${CI_API_V4_URL:-"https://gitlab.hrz.tu-chemnitz.de/api/v4"}
    CI_PROJECT_ID=${CI_PROJECT_ID:-"12649"}
    CI_PROJECT_NAME=${CI_PROJECT_NAME:-"bigdataframeworkconfigure"}
    CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-"$(git rev-parse --short HEAD)"}
    CI_COMMIT_REF_NAME="${CI_COMMIT_REF_NAME:-$(git branch --show-current)}"

    REPOSITORY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository"
    PACKAGE_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages"
    PACKAGE_REGISTRY_URL="${PACKAGE_URL}/generic/${CI_PROJECT_NAME}"
    RELEASE_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases"
    
    BDFC_TAR_FILE_NAME="${BDFC_EASYBUILD_PREFIX,,}-${VERSION}.tar.gz"
    BDFC_TAR_CHECKSUM_FILE_NAME="${BDFC_TAR_FILE_NAME}.sha512sum"
    BDFC_EASYBUILD_FILE_NAME="${BDFC_EASYBUILD_PREFIX}-${VERSION}.eb"

    if [[ "$COMMAND" == "create-package" ]]; then
        create_package "${VERSION}" "${MY_CI_TOKEN}"
    elif [[ "$COMMAND" == "delete-package" ]]; then
        delete_package "${VERSION}" "${MY_CI_TOKEN}"
    elif [[ "$COMMAND" == "create-release" ]]; then
        create_release "${VERSION}" "${MY_CI_TOKEN}"
    elif [[ "$COMMAND" == "delete-release" ]]; then
        delete_release "${VERSION}" "${MY_CI_TOKEN}"
    fi
}

main $@
